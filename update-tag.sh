#!/bin/bash

# Ensure yq is installed
if ! command -v yq &> /dev/null
then
    echo "yq could not be found. Please install it first."
    exit 1
fi

# Path to the YAML file
FILE_PATH="path_to_your_file.yaml"

# New image tag from CI/CD or any other source
NEW_TAG="new_image_tag"

# Update the tag value using yq
yq eval ".image.tag = \"$NEW_TAG\"" $FILE_PATH -i

# End
